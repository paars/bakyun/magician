package magician

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"
)

// GetGoPackageName uses go list to fetch the package name for the specified folder
func GetGoPackageName(path string) string {
	pkg, err := ExecOutput("go", "list", path)
	if err != nil {
		panic(err)
	}
	return pkg
}

// GitVersionLDFlags creates a go ld flags string for common build variables
func GitVersionLDFlags(pkg string) string {
	var git = "git"

	// GitCommit
	commit, _ := ExecOutput(git, "rev-parse", "--short", "HEAD")

	// GitBranch
	branch, _ := ExecOutput(git, "symbolic-ref", "-q", "--short", "HEAD")

	// GitState
	porcelain, _ := ExecOutput(git, "status", "--porcelain")
	state := "clean"
	if len(porcelain) > 0 {
		state = "dirty"
	}

	// GitSummary
	summary, _ := ExecOutput(git, "describe", "--always", "--tags", "--dirty")

	// BuildDate
	buildDate := time.Now().UTC().Format(time.RFC3339)

	// Version
	version := "0.0.0"
	homepage := ""
	file, err := os.Open("VERSION")
	if err != nil && !os.IsNotExist(err) {
		Error(fmt.Sprintf("Failed to read VERSION file: %v", err))
	}
	if err == nil {
		defer file.Close()
		scanner := bufio.NewScanner(file)

		scanner.Scan()
		sv := scanner.Text()
		if sv != "" {
			version = sv
		}

		if scanner.Scan() {
			homepage = scanner.Text()
		}

		if err := scanner.Err(); err != nil {
			Error(fmt.Sprintf("Failed to scan VERSION file: %v", err))
		}
	}

	// LongVersion
	longVersion := strings.Join([]string{version, commit, buildDate}, "-")
	if state == "dirty" {
		longVersion += "-dirty"
	}

	return fmt.Sprintf(
		`-X %v.GitCommit=%v -X %v.GitBranch=%v -X %v.GitState=%v -X %v.GitSummary=%v -X %v.BuildDate=%v -X %v.Version=%v -X %v.LongVersion=%v -X %v.Homepage=%v`,
		pkg, commit,
		pkg, branch,
		pkg, state,
		pkg, summary,
		pkg, buildDate,
		pkg, version,
		pkg, longVersion,
		pkg, homepage,
	)
}
