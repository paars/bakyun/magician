package magician

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/roger-king/color"
	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

const (
	echoFormat = "%v %v\n"
	quotes     = "\""

	doubleBits = 64
)

// Headline prints out a headline
func Headline(line string) {
	fmt.Fprintf(color.Output, echoFormat, color.CyanString("›"), color.HiYellowString(line))
}

// Echo prints out a simple line
func Echo(line string) {
	fmt.Fprintf(color.Output, echoFormat, color.CyanString("»"), line)
}

// Error prints out an error line
func Error(line string) {
	fmt.Fprintf(color.Output, echoFormat, color.RedString("»"), line)
}

func formatArgs(args []string) []string {
	formatted := make([]string, len(args))

	for idx, arg := range args {
		_, err := strconv.ParseBool(arg)
		isBool := err == nil
		_, err = strconv.ParseFloat(arg, doubleBits)
		isNumber := err == nil

		// string
		switch {
		case strings.Contains(arg, " "):
			formatted[idx] = color.BlueString(quotes + strings.Replace(arg, quotes, "\\"+quotes, -1) + quotes)
		case strings.HasPrefix(arg, "-"):
			formatted[idx] = color.HiBlackString(arg)
		case isBool:
			formatted[idx] = color.MagentaString(arg)
		case isNumber:
			formatted[idx] = color.YellowString(arg)
		default:
			formatted[idx] = color.BlueString(arg)
		}
	}

	return formatted
}

// EchoCommand prints out a fancified commandline
func EchoCommand(cmd string, args ...string) {
	line := append([]string{color.HiBlueString(cmd)}, formatArgs(args)...)
	Echo(strings.Join(line, " "))
}

// EchoCommandResult prints out a prettified exit status code
func EchoCommandResult(err error) {
	if mg.Verbose() {
		exitCode := sh.ExitStatus(err)
		exitString := color.GreenString("0")
		if exitCode != 0 {
			exitString = color.RedString("%v", exitCode)
		}

		Echo(exitString)
		if err != nil {
			Error(err.Error())
		}
	}
}
