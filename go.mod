module gitlab.com/bakyun/magician

go 1.14

require (
	github.com/magefile/mage v1.8.0
	github.com/mattn/go-colorable v0.1.0 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/roger-king/color v1.7.0
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
)
